/*
 * termo1.c
 *
 * Created: 2014-12-02 21:52:39
 *  Author: hexen2k mailto:hexen2k@gmail.com
 */ 


#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "onewire.h"
#include "ds18x20.h"

#define wait(x)	timer0=(x);while(timer0)
#define MAXSENSORS	1
#define TIMEOWCONVERSION 75	//75 * 10 ms = 750 ms
#define PULL_UP_ON()	PORTC&=~(1<<PC2)
#define PULL_UP_OFF()	PORTC|=1<<PC2

uint8_t seg0[] = {0x08, 0xEB, 0x90, 0x81, 0x43, 0x05, 0x04, 0x8B, 0x00, 0x01, 0xD7};
uint8_t seg1[] = {0x40, 0x4F, 0x22, 0x06, 0x0D, 0x14, 0x10, 0x4E, 0x00, 0x04, 0x3F};
volatile uint8_t timer0;
volatile uint8_t conversion_timer;
int16_t decicelsius;
int8_t decicelsius_mod;
uint8_t gSensorIDs[MAXSENSORS][OW_ROMCODE_SIZE];
uint8_t ow_fresh_flag, sensor_missing_flag;

void init(void);
void process_temperature(void);
static int8_t decicelsius_correct(int16_t val);
static uint8_t search_sensors(void);
void refresh_display(void);

int main(void) {	
	//int8_t i;
	init();

	sei();
    while(1)
    {	
		/*	display test
		for(i=-25; i <= 101; i++) {
			decicelsius_mod = i;
			ow_fresh_flag = 1;
			refresh_display();
			wait(100);
		}
		*/
		
		process_temperature();
		 
        refresh_display();
		
		//TODO:: Please write your application code 
    }
}

void init(void) {
	DDRB = 0xFF;
	DDRD = 0xFF;
	DDRC |= (1<<PC3) | (1<<PC2);	//minus sign, strong pull-up
	PULL_UP_OFF();
		
	//initial bar
	PORTB = seg0[10];
	PORTD = seg1[10];
	PORTC &= ~(1<<PC3);
	
	//timer2
	TCCR2 = (1<<CS22) | (1<<CS21) | (1<<CS20) | (1<<WGM21);	//prescaler=1024, CTC mode, 
	OCR2 = 78;	//T=1/(F_CPU/prescaler/79)= ~10m (interrupt every ~10ms)
	TIMSK = (1<<OCIE2);
}

ISR(TIMER2_COMP_vect) {
	uint8_t x;
	x = timer0;
	if(x) timer0 = --x;
	x = conversion_timer;
	if(x) conversion_timer = --x;
}

void process_temperature(void) {
	static uint8_t conversion_flag = 0;
	
	if(conversion_flag == 0) {
		if(search_sensors()) {	//temperature sensor present
			if(DS18X20_start_meas(DS18X20_POWER_PARASITE,NULL) == DS18X20_OK) {
				PULL_UP_ON();
				conversion_flag = 1;
				conversion_timer = TIMEOWCONVERSION;
			}
		}
		else {	//missing sensor
			sensor_missing_flag = 1;
		}
	}
	else if(conversion_timer==0) {	//after TIMEOWCONVERSION time (750 ms)
		PULL_UP_OFF();
		if (DS18X20_read_decicelsius(&gSensorIDs[0][0], &decicelsius) == DS18X20_OK) {						
			decicelsius_mod = decicelsius_correct(decicelsius);	//format data
			ow_fresh_flag = 1;
		}		
	conversion_flag = 0;
	}
}


static uint8_t search_sensors(void)
{
	uint8_t i;
	uint8_t id[OW_ROMCODE_SIZE];
	uint8_t diff, nSensors;

	ow_reset();
	nSensors = 0;

	diff = OW_SEARCH_FIRST;
	while ( diff != OW_LAST_DEVICE && nSensors < MAXSENSORS ) {
		DS18X20_find_sensor( &diff, &id[0] );

		if( diff == OW_PRESENCE_ERR ||  diff == OW_DATA_ERR) break;

		for ( i=0; i < OW_ROMCODE_SIZE; i++ )
		gSensorIDs[nSensors][i] = id[i];

		nSensors++;
	}

	return nSensors;
}

static int8_t decicelsius_correct(int16_t val){
	int8_t temp;
	temp = val/10;
	if(abs(val%10) >= 5) {
		if(val >= 0) temp++;
		else temp--;
	}
	return temp;
}

void refresh_display(void) {
	uint8_t dozens, unit, decicelsius_abs;
	 
	if(ow_fresh_flag) {
		
		decicelsius_abs = abs(decicelsius_mod);
		
		dozens = decicelsius_abs/10;
		if(dozens >= 10) dozens = 9;	//limiting scope
		unit = decicelsius_abs%10;
				
		PORTB = seg0[unit];
		
		if(dozens) {
			PORTD = seg1[dozens];					
		}
		else if(decicelsius_mod < 0) {
			PORTD = seg1[10];	//minus sign
		}
		else {
			PORTD = 0xFF;	//insignificant zero blanking
		}
		
		if(decicelsius_mod >= 0) {	//minus sign
			PORTC |= (1<<PC3);
		}
		else if(dozens) {
			PORTC &= ~(1<<PC3);
		}
		else {
			PORTC |= (1<<PC3);
		}

		ow_fresh_flag = 0;
	}
	else if(sensor_missing_flag) {
		PORTB = seg0[10];
		PORTD = seg1[10];
		PORTC &= ~(1<<PC3);
		sensor_missing_flag = 0;
	}
}
